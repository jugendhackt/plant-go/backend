// Configs
const configDB = require('./database');

// Database Query-Builder
const knex = require('knex')(configDB);

// or require in Node / Browserify
const Supercluster = require('supercluster');

const fs = require('fs');

async function convertTrees() {
    try {
        // load data from db
        let rows = await knex.from('trees').where({});
        console.log(` > send ${rows.length} trees...`);
        return rows;
    } catch (error) {
        console.error(error); // catch error
        return;
    }

}

convertTrees().then((data) => {
    let newData = [];
    data.forEach(d => {
        newData.push([d.lat, d.lon]);
    });

    x_max = Math.max.apply(Math, newData.map(function (o) { return o[0]; }))
    x_min = Math.min.apply(Math, newData.map(function (o) { return o[0]; }))

    y_max = Math.max.apply(Math, newData.map(function (o) { return o[1]; }))
    y_min = Math.min.apply(Math, newData.map(function (o) { return o[1]; }))


    console.log("x_max", x_max)
    console.log("x_min", x_min)
    console.log("y_max", y_max)
    console.log("y_min", y_min)

    const index = new Supercluster({
        map: (props) => ({ sum: props.myValue }),
        reduce: (accumulated, props) => { accumulated.sum += props.sum; }
    });




    fs.writeFile('trees.json', JSON.stringify(newData), (err) => {
        if (err) throw err;

        index.load(newData);
        console.log(newData.length)
        newNewData = index.getClusters([52, 52, 0, 0], 10);
        // console.log(index);
        fs.writeFile('trees.json', JSON.stringify(newNewData), (err) => {
            if (err) throw err;

            console.log(newNewData.length)
        });
    });
});

// Load DB config from file
const dbConfig = require('../database')

// Import libaries
const crypto = require('crypto')                            // needed for hasihing
const parser = require('csv-parse');                        // needed for parsing the csv file
const fs = require('fs')                                    // needed for file system access
const transformation = require('transform-coordinates')     // needed to transform EPSG:2398 coordinates to EPSG:4328 (WSG:84)
const knex = require('knex')(dbConfig)                      // library for db connection, give it the imported dbConfig
const commandLineArgs = require('command-line-args')        // libary for parsing cli arguments
const commandLineUsage = require('command-line-usage')

const transform = transformation('EPSG:2398', 'EPSG:4326')  // new Transform client from EPSG:2398 coordinates to EPSG:4328 (WSG:84)
const trees = []                                            // stores the trees to be inserted in to the database

let remoteHashTrees = {}                                    // Stores the trees fetched from the database, hashes are keys
let localHashTrees = {}                                     // Stores the trees from the csv, hashes are keys

let verbose = 0
let updates = 0                                             // used to collect statisticcommand-line-usages
let i = 0                                                   // counter in which element we are

const cliOptionDefinitions = [
    { name: 'verbose', alias: 'v', type: Boolean },
    { name: 'vv', type: Boolean },
    { name: 'vvv', type: Boolean },
    { name: 'quiet', alias: 'q', type: Boolean },
    { name: 'table', alias: 't', type: String, defaultValue: 'trees' },
    { name: 'source', type: String, defaultOption: true, defaultValue: 'data/kataster_alle.csv'},
    { name: 'help', alias: 'h', type: Boolean }
]

const cliOptions = commandLineArgs(cliOptionDefinitions)

const cliSections = [
    {
        header: "PlantGO CSV Parser",
        content: "Parses the CSV from OpenData (http://www.halle.de/de/Verwaltung/Online-Angebote/Offene-Verwaltungsdaten/Mit-Kartenbezug/index.aspx?ID=f2087a53-2c10-f7c5-4dba-9ad5112a90cb), cĺeans it and writes it to an MySQL Database.\nPart of the PlantGO game developed at JugendHackt Halle (Saale) 2019"
    },
    {
        header: 'Options',
        optionList: [
            {
                name: 'source',
                typeLabel: '{underline file}',
                description: 'The input csv to process.'
            },  
            {
                name: 'table',
                typeLabel: '{underline string}',
                description: 'The table in the database to use.',
                alias: 't'
            },
            {
                name: 'quiet',
                typeLabel: '{underline number}',
                description: 'Disable any info output.',
                alias: 'q'
            },
            {
                name: 'help',
                description: 'Print this usage guide and exit.',
                alias: 'h'
            },
            {
                name: 'verbose',
                typeLabel: '{underline number}',
                description: 'Enable verbose output.',
                alias: 'v'
            },
            {
                name: 'vv',
                typeLabel: '{underline number}',
                description: 'Enable extra verbose output.'
            },
            {
                name: 'vvv',
                typeLabel: '{underline number}',
                description: 'Enable extra very verbose output.'
            }
        ]
    }
]

const cliUsage = commandLineUsage(cliSections)

if (cliOptions.help)
{
    console.log(cliUsage)
    return
}

if (cliOptions.verbose) { verbose = 1 }
if (cliOptions.vv) { verbose = 2 }
if (cliOptions.vvv) { verbose = 3 }

if (cliOptions.quiet) { verbose = -1 }

const dataSource = cliOptions.source
const table = cliOptions.table

/**
 * Hashes the given string using the given algorithm
 * Returns the hash in hex format
 * 
 * @param {String} str The string to hash
 * @param {String} [sha1] algorithm The Algorithm to use
 * @returns {String} hex representation of the string
 */
function createHashFromString(str, algorithm) {
    if (!algorithm) {
        algorithm = 'sha1'
    }

    return crypto.createHash(algorithm).update(str).digest('hex');
}

/**
 * Generate a sha1 hash based on the lan and lot of the tree
 * 
 * @param {Object} tree
 */
function hashTreeObject(tree) {
    // Convert it to a float and round it to 15 digits after the decimal
    // Otherwise the (30;20) decimal from the database would generate a different hash
    const lat = parseFloat(tree.lat).toFixed(15)
    const lon = parseFloat(tree.lon).toFixed(15)

    // The '' forces the conversion to a string instead of adding the floats together
    return createHashFromString(lat + '' + lon)
}

/**
 * Counts the numbered parameters of the given object
 * 
 * @param {Object} tree
 */
function getValueCount(tree) {
    let valueCount = 0;

    for (let [key, value] in Object.entries(tree)) {
        if (value !== null) {
            valueCount++
        }
    }

    return valueCount
}

/**
 * Fetch all remote trees from the database and initialize the hash Object
 * 
 * @param {function} callback
 */
async function loadRemoteTrees(callback) {
    const startTime = new Date().getTime()
    if (verbose >= 1)
    {
        console.log('Starting to fetch RemoteTrees at ' + startTime)
    }
    knex(table)
        .select('*')
        .then((data) => {
            data.forEach((tr) => {
                remoteHashTrees[tr.hash] = tr               // Get the hashes from the database and store trees by that
            })
        })
        .then(() => {
            if (verbose >= 0)
            {
                console.info(Object.keys(remoteHashTrees).length + ' trees fetched from data in ' + (new Date().getTime() - startTime) + '')
            }
        })
        .then(callback)
}

const handleCsv = parser({ delimiter: ';' }, handleCsvData)

/**
 * Handles the Data that's coming from the CSV Handler
 * 
 * @param {*} err 
 * @param {*} data 
 */
async function handleCsvData(err, data) {
    data.shift()                                            // Remove the first dataset containing the headers
    knex.transaction(async function (trx) {                       // initialize new knex transaction
        if (verbose >= 0)
        {
            console.info(data.length + ' local trees found')
        }
        data.forEach(parseCsvElement)
        // Calculate the diff on ow many elements where duplicates or otherwise invalid
        if (verbose >= 0)
        {
            console.info(Object.keys(localHashTrees).length + ' unique local trees found (-' + (data.length - Object.keys(localHashTrees).length) + ')')
        }

        for (let hash in localHashTrees)                    // iterate over all unique and cleaned trees
        {
            if (!localHashTrees.hasOwnProperty(hash)) { return }

            i++

            const { tree } = localHashTrees[hash]             // get the tree from the hash object

            trx = syncWithRemote(tree, trx)
        }

        knex(table)                                         // mass-insert all trees
            .transacting(trx)
            .insert(trees)
            .catch((err) => {
                console.warn(err); throw err
            })

        return trx                                          // Return the trx Object for it to be handled
    })
        .catch((err) => {
            console.warn(err)                                   // Catch and display Errors
        })
        .finally(() => {
            if (verbose >= 0)
            {
                console.info(trees.length + ' Trees inserted');
                console.info(updates + ' Trees updated');
            }
            knex.destroy();
        })
    return;
}

/**
 * The Tree Element from the CSV
 * By creating a hash from the trees duplicate data is being detected and omitted
 * 
 * @param {Object} element CSV-Tree Element
 */
function parseCsvElement(element) {
    const tree = parseTreeElement(element)
    if (!tree) { return }

    const hash = hashTreeObject(tree)
    const valueCount = getValueCount(tree)                      // Count the not null values of the tree to determine which tree has more data
    const hashedTree = localHashTrees[hash]
    const hashedObject = {                                      // Store tree and the amount of not-null parameters in an object
        tree,
        valueCount
    }

    if (!hashedTree || hashedTree.valueCount < valueCount)      // Check if there is no previous tree with the same has of if it has fewer defined values
    {
        localHashTrees[hash] = hashedObject                     // Store the hashed tree in the localHashedTrees
    }
}

/**
 * Extract, cleanup and filter the relevant properties from the CSV Tree Element
 * 
 * @param {Object} element The Element from CSV
 */
function parseTreeElement(element) {
    const x_center = parseFloat(element[12])
    const y_center = parseFloat(element[11])

    if (element[6] === 'ja' || !x_center || !y_center) {        // Ignore if they do ont stand or don't have an x/y coordinate
        return false
    }

    const coordinates = transform.forward({ x: x_center, y: y_center }) // Transform the coordinates to the correct system

    let location_type = element[0]
    let body_circ = parseFloat(element[-2])
    let body_height = parseFloat(element[-1])
    let year = parseInt(element[2])
    let height = parseFloat(element[4])
    let type = element[8]
    let specific_type = element[9]
    let street = element[13]

    // Make data uniform by making all empty values null
    if (!body_circ || body_circ === 0) { body_circ = null }
    if (!body_height || body_height === 0) { body_height = null }
    if (!year || year === 0) { year = null }
    if (!height || height === 0) { height = null }
    if (!type || type === "") { type = null }
    if (!specific_type || specific_type === "") { specific_type = null }
    if (!street || street === "") { street = null }
    if (!location_type || location_type === "" || location_type === "keine Angaben") { location_type = null }

    // persist it to an object

    const tree = {
        lon: coordinates.x,
        lat: coordinates.y,
        location_type,
        street,
        body_circ,
        body_height,
        year,
        height,
        type,
        specific_type
    };

    tree.hash = hashTreeObject(tree)                        // Add the hash to identify the tree

    return tree
}

function syncWithRemote(tree, trx) {
    const remoteTree = remoteHashTrees[tree.hash]           // Select the corresponding tree on the remote
    
    let changesMade = false                             // track if any changes were added to the transaction for the current tree
    if (remoteTree) {
        
        if (verbose >= 1)
        {
            console.log(i, remoteTree.id, tree.hash, remoteTree.hash)
        }

        for (let property in tree)                          // Iterate over all properties to detect any differences between the remote and the local object
        {
            if (verbose >= 2) {
                console.log(i, tree[property] == remoteTree[property], property, tree[property], remoteTree[property])
            }

            if (verbose >= 3)
            {
                console.log(i,tree, remoteTree)
            }
            
            if (tree.hasOwnProperty(property) && !changesMade) // Only proceed if no changes have been stages previously
            {
                if (tree[property] != remoteTree[property])     // check if local tree and remote tree differ in the selected property
                {

                    changesMade = true
                    updates++

                    knex(table)
                        .transacting(trx)
                        .where('id', remoteTree.id)             // Update the remote object with that ID
                        .update(tree)
                        .finally(() => { return true })        // This is a wired bug that you have to have a finally in order for the update to be part of the transaction - I have no clue why

                    return trx;
                }
            }

        }
        return trx
    }
    else                                            // if the tree is not already in the database...
    {
        trees.push(tree)                            // ...mark it for insertion
    }

    return trx
}

/**
 * Loads and parses the csv file and starts all different update & comparison functions
 * 
 * @param {String} path path to the csv
 */
function loadAndParseFile(path) {
    fs.createReadStream(
        path,
        {
            encoding: 'binary',                             // By Default, the file is encoded as ISO 8859-2
        }
    ).pipe(handleCsv)
}


const io = require('@pm2/io');
io.action('db:update', (cb) => {
  clean.db(() => {
    loadRemoteTrees(() => {
        loadAndParseFile(dataSource)
    }).then(() => {
        return cb({ success: true });
    });
    return cb({ success: false });
  });
});


setInterval(() => {}, 1 << 30);
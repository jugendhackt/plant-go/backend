// Configs
const config = require('./config');
const configDB = require('./database');

// Express Web
const express = require('express');
const app = express();

const swaggerUi = require('swagger-ui-express');
const YAML = require('yamljs')
const swaggerDocument = YAML.load('./openapi.yaml');

// Process Manager 2 ->  https://app.pm2.io/#/r/e3dyffp2l2lh1mz
  /*x
  const io = require('@pm2/io');
  io.action('db:clean', (cb) => {
    clean.db(() => {
      // cb must be called at the end of the action
      return cb({ success: true });
    });
  });*/


// Api Docs 
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

var bodyParser = require("body-parser"); 
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// Cooldown
let time = 0;

// Database Query-Builder
const knex = require('knex')(configDB);

// Additions
const consola = require('consola')

// ## Conslosion ##
function createConslosion(req) {
  // console.log(req)
  consola.log(`[${req.connection.remoteAddress || req.headers['x-forwarded-for']}] => "${req.hostname}${req.url}"`)
  if (req.query === {})
    consola.info(req.query);
  else
    consola.info(req.body)
  return;
}

function shuffle(a) {
  var j, x, i;
  for (i = a.length - 1; i > 0; i--) {
      j = Math.floor(Math.random() * (i + 1));
      x = a[i];
      a[i] = a[j];
      a[j] = x;
  }
  return a;
}

hashCode = function (x) {
  var hash = 0, i, chr;
  if (x.length === 0) return hash;
  for (i = 0; i < x.length; i++) {
    chr = x.charCodeAt(i);
    hash = ((hash << 5) - hash) + chr;
    hash |= 0; // Convert to 32bit integer
  }
  return hash;
};

// ### Listensers ####

app.get('/favicon.ico', function (req, res) {
  res.sendFile('/home/plant-go/backend/pages/favicon.ico');
});

app.post('/users/', async function (req, res) {
  createConslosion(req);

  knex.from('users').where({ id: req.body.user_id }).then(data => {
    if (data.length !== 0)
      knex.from('users').where({ username: req.body.user_id }).update({ team: req.body.team_id }).then(data_x => {
        res.status(200).send({ message: 'user updated! :)' })
        consola.log(` > update ${req.body.username} in ${req.path.split('/')[1]}...`);

      });
    else
      res.status(300).send({ message: 'wrong login :(' })
    });
});

app.get('/users/login/', async function (req, res) {
  createConslosion(req);

  const username = req.query.username,
    password = hashCode(req.query.password);

  let sessionKey = `${username}${password}${Date.now}`
  sessionKey = hashCode(sessionKey);

  knex.from('users').where({ username: username, password: password }).then(data => {
    if (data.length !== 0)
      knex.from('users').where({ username: username, password: password }).update({ sessionKey: sessionKey }).then(data_x => {
        res.status(200).send({ message: 'login!', sessionKey: sessionKey })
        consola.log(` > update ${username} in ${req.path.split('/')[1]}...`);
      });
    else
      res.status(300).send({ message: 'wrong login :(' })
  });
});

app.get('/users/create/', async function (req, res) {
  createConslosion(req);

  const username = req.query.username,
    password = hashCode(req.query.password);

  knex.from('users').where({ username: username }).then(data => {
    consola.log(data.length)
    if (data.length === 0) {
      let sessionKey = `${username}${password}${Date.now}`
      sessionKey = hashCode(sessionKey);

      knex('users').insert({ username: username, password: password, sessionKey: sessionKey }).then(() => {
        res.status(200).send({ message: 'succ', sessionKey: sessionKey });
      });
    } else {
      res.send({ statusCode: 100, message: 'name already used' });
    }
  });
});

app.get('/trees/range/', async function (req, res) {
  if ((Date.now() - time) < config.request_cooldown)
    return (res.status(429).send({ sqlMessage: 'please wait!' }));
  time = Date.now()
  createConslosion(req);

  try {

    
    // load data from db
    let rows = await knex(req.path.split('/')[1]).where('lat', '>', req.query.y1).andWhere('lat', '<', req.query.y2).andWhere('lon', '>', req.query.x1).andWhere('lon', '<', req.query.x2);

    if(req.query.limit)
      rows = shuffle(rows).slice(0, req.query.limit);
    res.send(rows);
    consola.log(` > send ${rows.length} ${req.path.split('/')[1]}...`);
  } catch (error) {

    // catch errors 
    res.status(400).send(error);
    consola.error(error);
  }
});

app.get('/trees/irrigation/', async function (req, res) {
  if ((Date.now() - time) < config.request_cooldown)
    return (res.status(429).send({ sqlMessage: 'please wait!' }));
  time = Date.now()
  createConslosion(req);

  try {

    // load data from db
    let rows = await knex.from(req.path.split('/')[1]);

    // filter trees in area
    rows = rows.filter(row => {
      return ((row.lon < req.query.y1 && row.lon > req.query.y2) && (row.lat < req.query.x1 && row.lat > req.query.x2))
    });

    consola.log(` > send ${rows.length} ${req.path.split('/')[1]}...`);
    res.send(rows);
  } catch (error) {

    // catch errors 
    consola.error(error);
    res.send(error);
  }
});

// ## Simple Search ##
app.get('/*/', async function (req, res) {
  if (!req.path.split('/')[1]) {
    res.redirect(301, 'https://jugendhackt.gitlab.io/plant-go/frontend/');
    return;
  }

  if (req.path.split('/')[1] === 'api-doc')
    return (res.send(swaggerUi.setup(swaggerDocument)));

  if ((Date.now() - time) < config.request_cooldown)
    return (res.send({ sqlMessage: 'please wait!' }));
  time = Date.now()

  if (req.path.split('/')[1] === 'users')
    return (res.status(403).send({ message: 'do not even try ;)' }))

  if (req.path.split('/')[2] === 'find')
    return (res.status(404).send({ message: 'not found :( just dont use "find"' }))

  if (req.path.split('/')[2])
    return;

  createConslosion(req);
  try {

    // load data from db
    let rows = await knex.from(req.path.split('/')[1]).where(req.query);

    consola.log(` > send ${rows.length} ${req.path.split('/')[1]}...`);
    res.send(rows);
  } catch (error) {

    // catch error
    consola.error(error);
    res.send(error);
  }
});

app.listen(config.port, function () {
  consola.success(`App listening on port ${config.port}!`);
});
